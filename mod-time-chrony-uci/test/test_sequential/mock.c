/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "mock.h"

static NTP_Leap leap_status = LEAP_Normal;
static uint32_t n_sources = 0;
static uint16_t requested_cmd = 0;
static uint16_t timeout_counter = 0;
static IPAddr ip_addr = {.family = IPADDR_INET4, .addr.in4 = 0x7BC89FA2}; /* 123.200.159.162 */
static int current_sock = -1;

ssize_t __wrap_send(int sockfd, const void* buf, size_t len, int flags) {
    CMD_Request* request = (CMD_Request*) buf;
    requested_cmd = ntohs(request->command);
    return sockfd == current_sock ? 1 : __real_send(sockfd, buf, len, flags);
}

ssize_t __wrap_recv(UNUSED int sockfd, void* buf, UNUSED size_t len, UNUSED int flags) {
    CMD_Reply* reply = (CMD_Reply*) buf;
    if(requested_cmd == REQ_TRACKING) {
        reply->reply = htons(RPY_TRACKING);
        reply->data.tracking.leap_status = htons(leap_status);
    } else if(requested_cmd == REQ_N_SOURCES) {
        reply->reply = htons(RPY_N_SOURCES);
        reply->data.n_sources.n_sources = htonl(n_sources);
    } else if(requested_cmd == REQ_SOURCE_DATA) {
        reply->reply = htons(RPY_SOURCE_DATA);
        reply->data.source_data.ip_addr.family = htons(ip_addr.family);
        reply->data.source_data.ip_addr.addr.in4 = htonl(ip_addr.addr.in4);
        reply->data.source_data.timeout_counter = htons(timeout_counter);
    }

    return 1;
}

int __wrap_bind(UNUSED int sockfd, UNUSED const struct sockaddr* addr, UNUSED socklen_t addrlen) {
    return 0;
}

int __wrap_chmod(UNUSED const char* path, UNUSED mode_t mode) {
    return 0;
}

int __wrap_connect(UNUSED int sockfd, UNUSED const struct sockaddr* addr, UNUSED socklen_t addrlen) {
    return 0;
}

int __wrap_unix_socket(void) {
    int sock = __real_unix_socket();
    if(sock >= 0) {
        current_sock = sock;
    }

    return sock;
}

int __wrap_close(int fd) {
    if(fd == current_sock) {
        current_sock = -1;
    }

    return __real_close(fd);
}

int __wrap_wait_for_response(int sock) {
    return sock == current_sock ? 1 : __real_wait_for_response(sock);
}

void uti_build_chrony_reply(NTP_Leap leap, uint32_t n_src, uint16_t timeout) {
    leap_status = leap;
    n_sources = n_src;
    timeout_counter = timeout;
}

void uti_assert_source_correct(const char* source) {
    char ch[1000];
    FILE* fptr = fopen(ACTIVE_SOURCES_FILE, "r");
    if(fptr == NULL) {
        fail_msg("Error: %s does not exist", ACTIVE_SOURCES_FILE);
        return;
    }

    fscanf(fptr, "%[^\n]", ch);
    fclose(fptr);

    assert_string_equal(source, ch);
}

void uti_assert_source_doesnt_exist(void) {
    assert_null(fopen(ACTIVE_SOURCES_FILE, "r"));
}
