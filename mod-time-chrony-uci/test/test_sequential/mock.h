/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __COMMON_MOCK_H__
#define __COMMON_MOCK_H__

#include <unistd.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp_subproc.h>
#include <amxc/amxc_macros.h>

#include "sequential.h"

#include "../../include_priv/chronyc.h"

typedef enum {
    LEAP_Normal = 0,
    LEAP_InsertSecond = 1,
    LEAP_DeleteSecond = 2,
    LEAP_Unsynchronised = 3
} NTP_Leap;

ssize_t __wrap_recv(int sockfd, void* buf, size_t len, int flags);

ssize_t __wrap_send(int sockfd, const void* buf, size_t len, int flags);
ssize_t __real_send(int sockfd, const void* buf, size_t len, int flags);
int __wrap_bind(int sockfd, const struct sockaddr* addr, socklen_t addrlen);
int __wrap_chmod(const char* path, mode_t mode);
int __wrap_connect(int sockfd, const struct sockaddr* addr, socklen_t addrlen);
int __wrap_unix_socket(void);
int __real_unix_socket(void);
int __wrap_close(int fd);
int __real_close(int fd);
int __wrap_wait_for_response(int sock);
int __real_wait_for_response(int sock);
void uti_build_chrony_reply(NTP_Leap leap, uint32_t n_src, uint16_t timeout);
void uti_assert_source_correct(const char* source);
void uti_assert_source_doesnt_exist(void);

#endif // __COMMON_MOCK_H__
