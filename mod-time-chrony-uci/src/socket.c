/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <sys/time.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <sys/stat.h>

#include "socket.h"
#include "mod_time_chrony.h"

#define CLEAN_FD(fd) close(fd); fd = -1
#define CHRONY_SOCK "/var/run/chrony/chronyd.sock"
#define TIME_MGR_SOCK "/var/run/chrony/time_mgr.sock"

static bool set_nonblock(int socket) {
    int flags = fcntl(socket, F_GETFL, 0);
    int ret = -1;
    bool rc = false;

    ret = fcntl(socket, F_SETFL, flags | O_NONBLOCK);
    when_true_trace(ret == -1, exit, ERROR, "Failed to set socket non blocking: (%d) - %s",
                    errno, strerror(errno));

    rc = true;

exit:
    return rc;
}

int unix_socket(void) {
    struct sockaddr_un addr;
    int sock = -1;
    bool close_socket = false;

    memset(&addr, 0, sizeof(struct sockaddr_un));

    sock = socket(AF_UNIX, SOCK_DGRAM, 0);
    when_true_trace(sock < 0, exit, ERROR, "Failed to open socket: (%d) - %s",
                    errno, strerror(errno));

    when_false_status(set_nonblock(sock), exit, close_socket = true);

    unlink(TIME_MGR_SOCK);
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, TIME_MGR_SOCK, sizeof(addr.sun_path) - 1);

    if(bind(sock, (const struct sockaddr*) &addr, sizeof(addr)) < 0) {
        SAH_TRACEZ_ERROR(ME, "Cannot bind socket \"%s\": (%d) - %s",
                         TIME_MGR_SOCK, errno, strerror(errno));
        close_socket = true;
        goto exit;
    }

    if(chmod(TIME_MGR_SOCK, 0666) < 0) {
        SAH_TRACEZ_ERROR(ME, "Cannot chmod 0666 \"%s\": (%d) - %s",
                         TIME_MGR_SOCK, errno, strerror(errno));
        close_socket = true;
        goto exit;
    }

    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, CHRONY_SOCK, sizeof(addr.sun_path) - 1);

    if(connect(sock, (const struct sockaddr*) &addr, sizeof(addr)) == -1) {
        SAH_TRACEZ_ERROR(ME, "Cannot connect to chronyd socket \"%s\": (%d) - %s",
                         CHRONY_SOCK, errno, strerror(errno));
        close_socket = true;
    }

exit:
    if(close_socket) {
        close(sock);
        sock = -1;
    }

    return sock;
}

int wait_for_response(int sock) {
    int ret = -1;
    struct timeval tv = {
        .tv_sec = 5,
        .tv_usec = 0
    };

    fd_set fds;
    memset(&fds, 0, sizeof(fd_set));

    FD_SET(sock, &fds);
    ret = select(sock + 1, &fds, NULL, NULL, &tv);
    if(ret == -1) {
        SAH_TRACEZ_ERROR(ME, "Select failed: (%d) - %s", errno, strerror(errno));
    }

    return ret;
}
