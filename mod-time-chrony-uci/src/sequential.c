/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "sequential.h"
#include "chronyc.h"
#include <math.h>

#define ME "sequential"
#define POLL_MINVALUE 64
#define POLL_MAXVALUE 131072
#define UNSYNCHRONISED 3

typedef struct _ntp_sync_interval_t {
    uint32_t value;
    uint8_t log2value;
} ntp_sync_interval_t;

static bool sequential_mode = false;
static uint32_t sequential_mode_timeout = 0;
static uint32_t ntp_retry_interval = 0;
static ntp_sync_interval_t ntp_sync_interval = {.value = 64, .log2value = 6};
static amxp_timer_t* ntp_retry_timer = NULL;
static amxp_timer_t* reload_delay_timer = NULL;
static amxp_timer_t* sync_check_timer = NULL;
static amxc_llist_t sources;
static amxc_llist_it_t* source_in_use_it = NULL; // iterator of the source currently used

static void sync_check(uint32_t value) {
    SAH_TRACEZ_INFO(ME, "Sync check within [%ums]", value);
    amxp_timer_start(sync_check_timer, value);
}

static void update_ntp_sync_interval(uint32_t value) {
    if(value < POLL_MINVALUE) {
        SAH_TRACEZ_ERROR(ME, "%u lower than min value %u", value, POLL_MINVALUE);
        value = POLL_MINVALUE;
    } else if(value > POLL_MAXVALUE) {
        SAH_TRACEZ_ERROR(ME, "%u higher than max value %u", value, POLL_MAXVALUE);
        value = POLL_MAXVALUE;
    }

    ntp_sync_interval.value = value;
    ntp_sync_interval.log2value = log2(value);

    SAH_TRACEZ_INFO(ME, "ntp sync interval set to %u(%u)", ntp_sync_interval.value, ntp_sync_interval.log2value);
}

static bool is_ntp_synchronised(void) {
    bool is_sync = false;
    amxc_var_t tracking_result;
    amxc_var_t sources_result;
    uint32_t leap_status = 0;
    bool all_sources_timeout = true;

    amxc_var_init(&tracking_result);
    amxc_var_set_type(&tracking_result, AMXC_VAR_ID_HTABLE);

    amxc_var_init(&sources_result);
    amxc_var_set_type(&sources_result, AMXC_VAR_ID_LIST);

    when_failed_trace(chronyc_cmd_tracking(&tracking_result), exit, ERROR, "Failed to get tracking result");
    when_failed_trace(chronyc_cmd_sources(&sources_result), exit, ERROR, "Failed to get tracking result");

    leap_status = GET_UINT32(&tracking_result, "leap_status");

    amxc_var_for_each(source, &sources_result) {
        if(GET_UINT32(source, "timeout_counter") == 0) {
            SAH_TRACEZ_INFO(ME, "at least one source %s is still valid", GET_CHAR(source, "name"));
            all_sources_timeout = false;
            break;
        }
    }

    SAH_TRACEZ_INFO(ME, "leap status [%u] - all sources timeout [%s]", leap_status, all_sources_timeout ? "yes" : "no");

    if((leap_status != UNSYNCHRONISED) && (all_sources_timeout == false)) {
        is_sync = true;
    }

exit:
    amxc_var_clean(&tracking_result);
    amxc_var_clean(&sources_result);
    return is_sync;
}

static const char* get_source_type(const char* source) {
    return strstr(source, ".pool.") != NULL ? "pool" : "server";
}

void sequential_configure(amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t* time_clients = NULL;

    amxc_llist_clean(&sources, amxc_string_list_it_free);
    source_in_use_it = NULL;
    amxc_llist_init(&sources);

    sequential_mode = GETP_BOOL(data, "Time.sequential_mode");
    sequential_mode_timeout = GETP_UINT32(data, "Time.sequential_mode_timeout");
    update_ntp_sync_interval(GETP_UINT32(data, "Time.ntp_sync_interval"));
    ntp_retry_interval = GETP_UINT32(data, "Time.ntp_retry_interval");

    time_clients = GET_ARG(data, "Clients");
    amxc_var_for_each(client, time_clients) {
        bool enable = GET_BOOL(client, "enable");
        if(enable == true) {
            bool iburst = GET_BOOL(client, "iburst");
            int32_t ipversion = GET_INT32(client, "ipversion");
            amxc_var_t* client_sources = GET_ARG(client, "servers");
            amxc_var_for_each(source, client_sources) {
                amxc_string_t tmp;
                amxc_string_init(&tmp, 64);
                amxc_string_appendf(&tmp, "%s %s", get_source_type(GET_CHAR(source, NULL)), GET_CHAR(source, NULL));
                if(iburst == true) {
                    amxc_string_appendf(&tmp, " %s", "iburst");
                }
                if(ipversion == 4) {
                    amxc_string_appendf(&tmp, " %s", "ipv4");
                } else if(ipversion == 6) {
                    amxc_string_appendf(&tmp, " %s", "ipv6");
                }
                amxc_string_appendf(&tmp, " minpoll %d", ntp_sync_interval.log2value);
                amxc_string_appendf(&tmp, " maxpoll %d", ntp_sync_interval.log2value);
                amxc_llist_add_string(&sources, amxc_string_get(&tmp, 0));
                amxc_string_clean(&tmp);
            }
        }
    }

    SAH_TRACEZ_OUT(ME);
}

static void chrony_reload_source(const char* source) {
    SAH_TRACEZ_IN(ME);
    FILE* fptr = NULL;

    when_str_empty_trace(source, exit, ERROR, "Source null or empty");

    SAH_TRACEZ_APP_INFO(ME, "Reload source: %s", source);

    fptr = fopen(ACTIVE_SOURCES_FILE, "w");
    when_null_trace(fptr, exit, ERROR, "Failed to open %s", ACTIVE_SOURCES_FILE);

    /* update *.sources file */
    fprintf(fptr, "%s\n", source);
    fclose(fptr);

    /* ask chrony daemon to Re-read *.sources files */
    if(chronyc_cmd_reload() != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to reload sources");
    }

exit:
    SAH_TRACEZ_OUT(ME);
}

static void active_sources_reload(void) {
    SAH_TRACEZ_IN(ME);
    amxc_string_t* source_in_use = NULL;

    /* stop timers that could trigger a reload */
    amxp_timer_stop(reload_delay_timer);
    amxp_timer_stop(sync_check_timer);
    amxp_timer_stop(ntp_retry_timer);

    if(amxc_llist_is_empty(&sources) == true) {
        SAH_TRACEZ_APP_INFO(ME, "Source's list empty, do nothing");
        source_in_use_it = NULL;
        goto exit;
    }

    source_in_use_it = (source_in_use_it == NULL) ? amxc_llist_get_first(&sources) : amxc_llist_it_get_next(source_in_use_it);

    if(source_in_use_it == NULL) {
        remove(ACTIVE_SOURCES_FILE);
        if(chronyc_cmd_reload() != 0) {
            SAH_TRACEZ_ERROR(ME, "Failed to reload sources");
        }
        SAH_TRACEZ_APP_INFO(ME, "Last source has been used, retry within %d seconds", ntp_retry_interval);
        amxp_timer_start(ntp_retry_timer, ntp_retry_interval * 1000);
        goto exit;
    }

    source_in_use = amxc_llist_it_get_data(source_in_use_it, amxc_string_t, it);
    chrony_reload_source(amxc_string_get(source_in_use, 0));
    sync_check(sequential_mode_timeout);
exit:
    SAH_TRACEZ_OUT(ME);
}

void sequential_start(void) {
    SAH_TRACEZ_IN(ME);
    if(sequential_mode == false) {
        SAH_TRACEZ_APP_INFO(ME, "Sequential mode disabled");
    } else {
        SAH_TRACEZ_APP_INFO(ME, "Sequential mode enabled");
        SAH_TRACEZ_APP_INFO(ME, "SyncInterval [%us (%u)] - RetryInterval [%ds]", ntp_sync_interval.value, ntp_sync_interval.log2value, ntp_retry_interval);
        SAH_TRACEZ_APP_INFO(ME, "Sources List:");
        amxc_llist_iterate(it, &sources) {
            amxc_string_t* source = amxc_llist_it_get_data(it, amxc_string_t, it);
            SAH_TRACEZ_APP_INFO(ME, "-> %s", amxc_string_get(source, 0));
        }
        // wait for chrony daemon to be ready
        SAH_TRACEZ_APP_INFO(ME, "delay first reload for 5s");
        amxp_timer_start(reload_delay_timer, 5000);
    }
    SAH_TRACEZ_OUT(ME);
}

void sequential_stop(void) {
    SAH_TRACEZ_IN(ME);
    amxp_timer_stop(reload_delay_timer);
    amxp_timer_stop(ntp_retry_timer);
    amxp_timer_stop(sync_check_timer);

    source_in_use_it = NULL;

    remove(ACTIVE_SOURCES_FILE);
    SAH_TRACEZ_OUT(ME);
}

static void ntp_retry_timer_timeout(UNUSED amxp_timer_t* timer, UNUSED void* userdata) {
    SAH_TRACEZ_APP_INFO(ME, "NTP Retry timer expired");
    active_sources_reload();
}

static void reload_delay_timer_timeout(UNUSED amxp_timer_t* timer, UNUSED void* userdata) {
    SAH_TRACEZ_APP_INFO(ME, "Reload delay timer expired");
    active_sources_reload();
}

static void sync_check_timer_timeout(UNUSED amxp_timer_t* timer, UNUSED void* userdata) {
    if(is_ntp_synchronised() == false) {
        SAH_TRACEZ_APP_INFO(ME, "NTP unsynchronised");
        active_sources_reload();
    } else {
        SAH_TRACEZ_INFO(ME, "NTP synchronised");
        // check synchronisation state each sequential_mode_timeout ms
        sync_check(sequential_mode_timeout);
    }
}

void sequential_init(void) {
    SAH_TRACEZ_IN(ME);
    when_failed_trace(amxp_timer_new(&ntp_retry_timer, ntp_retry_timer_timeout, NULL), exit, ERROR, "Failed to create ntp retry timer");
    when_failed_trace(amxp_timer_new(&reload_delay_timer, reload_delay_timer_timeout, NULL), exit, ERROR, "Failed to create reload delay timer");
    when_failed_trace(amxp_timer_new(&sync_check_timer, sync_check_timer_timeout, NULL), exit, ERROR, "Failed to create sync check timer");
    amxc_llist_init(&sources);
exit:
    SAH_TRACEZ_OUT(ME);
}

void sequential_exit(void) {
    SAH_TRACEZ_IN(ME);
    amxp_timer_delete(&ntp_retry_timer);
    amxp_timer_delete(&reload_delay_timer);
    amxp_timer_delete(&sync_check_timer);
    amxc_llist_clean(&sources, amxc_string_list_it_free);
    SAH_TRACEZ_OUT(ME);
}
