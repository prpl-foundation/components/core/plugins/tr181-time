/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <debug/sahtrace.h>
#include "uci.h"

#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxb/amxb.h>
#include <stdio.h>
#include <stdlib.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include "mod_time_chrony.h"
#include "mod_time_uci.h"
#include "chrony.h"
#include "chronyc.h"
#include "service.h"
#include "utils.h"
#include "sequential.h"

static int service_terminate(UNUSED const char* function_name,
                             UNUSED amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    ntp_daemon_service_terminate();
    return 0;
}

static int service_state(UNUSED const char* function_name,
                         UNUSED amxc_var_t* args,
                         UNUSED amxc_var_t* ret) {
    return get_sync_state();
}

static int daemon_init(UNUSED const char* function_name,
                       UNUSED amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    return chrony_init();
}
static int service_running(UNUSED const char* function_name,
                           UNUSED amxc_var_t* args,
                           UNUSED amxc_var_t* ret) {
    return ntp_daemon_service_running() ? 0 : -1;
}

static int update_ntp_config(UNUSED const char* function_name,
                             amxc_var_t* args,
                             amxc_var_t* ret) {
    int retval = -1;

    retval = align_config(args, ret);
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "align chrony config failed");
    } else if((retval > 0) || (!ntp_daemon_service_running())) {
        SAH_TRACEZ_INFO(ME, "align config changed or daemon not running: forcing a restart");
        apply_config(GETP_BOOL(args, "Time.enable"));
        retval = 0;
    } else {
        SAH_TRACEZ_INFO(ME, "No change to chrony config");
        if(GETP_BOOL(args, "Time.sequential_mode")) {
            /* In sequential mode we don't expect the config to change, but
             * still need to restart */
            sequential_stop();
            sequential_start();
        }
    }

    return retval;
}

static int reload_sources(UNUSED const char* function_name,
                          amxc_var_t* args,
                          UNUSED amxc_var_t* ret) {
    bool onoff = GET_BOOL(args, "onoff");

    if(GETP_BOOL(args, "Time.sequential_mode")) {
        apply_config(GETP_BOOL(args, "Time.enable"));
    } else {
        uint32_t sources = 0;
        when_false(ntp_daemon_service_running(), exit);

        chronyc_cmd_onoffline(onoff);
        when_false(onoff, exit);
        if((chronyc_cmd_nsources(&sources) != 0) || (sources == 0)) {
            SAH_TRACEZ_INFO(ME, "(Re)applying the chronyd config");
            apply_config(true);
        } else {
            chronyc_cmd_reload();
            chronyc_cmd_refresh();
        }
    }

exit:
    return 0;
}

static AMXM_CONSTRUCTOR chrony_uci_module_start(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    amxm_module_register(&mod, so, MOD_TIME_CTRL);
    amxm_module_add_function(mod, "update-ntp-config", update_ntp_config);
    amxm_module_add_function(mod, "reload-sources", reload_sources);
    amxm_module_add_function(mod, "init-daemon", daemon_init);
    amxm_module_add_function(mod, "service-running", service_running);
    amxm_module_add_function(mod, "service-terminate", service_terminate);
    amxm_module_add_function(mod, "service-state", service_state);

    sequential_init();
    return 0;
}

static AMXM_DESTRUCTOR chrony_uci_module_stop(void) {
    sequential_exit();
    return 0;
}
