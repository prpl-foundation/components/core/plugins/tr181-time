/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__CHRONYC_H__)
#define __CHRONYC_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

#include <amxc/amxc.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

typedef struct {
    union {
        uint32_t in4;
        uint8_t in6[16];
        uint32_t id;
    } addr;
    uint16_t family;
    uint16_t _pad;
} IPAddr;

/* Structure used to exchange timespecs independent of time_t size */
typedef struct {
    uint32_t tv_sec_high;
    uint32_t tv_sec_low;
    uint32_t tv_nsec;
} Timespec;

/* 32-bit floating-point format consisting of 7-bit signed exponent
   and 25-bit signed coefficient without hidden bit.
   The result is calculated as: 2^(exp - 25) * coef */
typedef struct {
    int32_t f;
} Float;

#define IPADDR_UNSPEC 0
#define IPADDR_INET4 1
#define IPADDR_INET6 2
#define IPADDR_ID 3

#define MAX_PADDING_LENGTH 484
#define PROTO_VERSION_PADDING 6
#define PROTO_VERSION_NUMBER 6

#define PKT_TYPE_CMD_REQUEST 1
#define PKT_TYPE_CMD_REPLY 2

/* Status codes */
#define STT_SUCCESS 0
#define STT_FAILED 1
#define STT_UNAUTH 2
#define STT_INVALID 3
#define STT_ACCESSDENIED 9
#define STT_BADPKTVERSION 18
#define STT_BADPKTLENGTH 19

/* Request */
#define REQ_NULL 0
#define REQ_ONLINE 1
#define REQ_OFFLINE 2
#define REQ_N_SOURCES 14
#define REQ_SOURCE_DATA 15
#define REQ_TRACKING 33
#define REQ_REFRESH 53
#define REQ_RESET_SOURCES 66
#define REQ_RELOAD_SOURCES 70
/* Reply */
#define RPY_NULL 1
#define RPY_N_SOURCES 2
#define RPY_SOURCE_DATA 3
#define RPY_TRACKING 5

typedef struct {
    int32_t EOR;
} REQ_Null;

typedef struct {
    int32_t index;
    int32_t EOR;
} REQ_Source_Data;

typedef struct {
    uint8_t version;  /* Protocol version */
    uint8_t pkt_type; /* What sort of packet this is */
    uint8_t res1;
    uint8_t res2;
    uint16_t command;  /* Which command is being issued */
    uint16_t attempt;  /* How many resends the client has done
                                (count up from zero for same sequence
                                number) */
    uint32_t sequence; /* Client's sequence number */
    uint32_t pad1;
    uint32_t pad2;

    union {
        REQ_Null null;
        REQ_Source_Data source_data;
    } data; /* Command specific parameters */

    /* Padding used to prevent traffic amplification.  It only defines the
       maximum size of the packet, there is no hole after the data field. */
    uint8_t padding[MAX_PADDING_LENGTH];

} CMD_Request;

typedef struct {
    int32_t EOR;
} RPY_Null;

typedef struct {
    uint32_t n_sources;
    int32_t EOR;
} RPY_N_Sources;

typedef struct {
    IPAddr ip_addr;
    int16_t poll;
    uint16_t stratum;
    uint16_t state;
    uint16_t mode;
    uint16_t flags;
    uint16_t reachability;
    uint32_t since_sample;
    Float orig_latest_meas;
    Float latest_meas;
    Float latest_meas_err;
    uint16_t timeout_counter;
    int32_t EOR;
} RPY_Source_Data;

typedef struct {
    uint32_t ref_id;
    IPAddr ip_addr;
    uint16_t stratum;
    uint16_t leap_status;
    Timespec ref_time;
    Float current_correction;
    Float last_offset;
    Float rms_offset;
    Float freq_ppm;
    Float resid_freq_ppm;
    Float skew_ppm;
    Float root_delay;
    Float root_dispersion;
    Float last_update_interval;
    int32_t EOR;
} RPY_Tracking;

typedef struct {
    uint8_t version;
    uint8_t pkt_type;
    uint8_t res1;
    uint8_t res2;
    uint16_t command; /* Which command is being replied to */
    uint16_t reply;   /* Which format of reply this is */
    uint16_t status;  /* Status of command processing */
    uint16_t pad1;    /* Padding for compatibility and 4 byte alignment */
    uint16_t pad2;
    uint16_t pad3;
    uint32_t sequence; /* Echo of client's sequence number */
    uint32_t pad4;
    uint32_t pad5;

    union {
        RPY_Null null;
        RPY_N_Sources n_sources;
        RPY_Source_Data source_data;
        RPY_Tracking tracking;
    } data; /* Reply specific parameters */

} CMD_Reply;

int chronyc_cmd_tracking(amxc_var_t* result);
int chronyc_cmd_reload(void);
int chronyc_cmd_refresh(void);
int chronyc_cmd_onoffline(bool onoff);
int chronyc_cmd_nsources(uint32_t* n_sources);
int chronyc_cmd_sources(amxc_var_t* result);

#ifdef __cplusplus
}
#endif

#endif // __CHRONYC__H
