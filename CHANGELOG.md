# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.7.13 - 2025-01-06(12:30:29 +0000)

### Other

- [NTP] Add IPVersion parameter to Time.Client instances

## Release v2.7.12 - 2024-12-19(15:50:30 +0000)

## Release v2.7.11 - 2024-12-11(14:55:13 +0000)

### Other

- [NTP] Add SequentialModeTimeout parameter in Time object

## Release v2.7.10 - 2024-11-28(13:20:49 +0000)

### Fixes

- [time-manager] Chrony daemon's configuration is not always applied correctly

## Release v2.7.9 - 2024-11-19(09:39:09 +0000)

### Fixes

- [time-manager] State should go to 'Error' after a number of unsuccessful attempts

## Release v2.7.8 - 2024-11-18(12:09:53 +0000)

### Fixes

- [time-manager] Rename default client instance

### Other

- [time-manager] chrony daemon takes too long (~7s) to go to sync state

## Release v2.7.7 - 2024-10-10(10:40:31 +0000)

### Other

- [Time-manager] Use unix domain socket instead of chronyc

## Release v2.7.6 - 2024-10-10(10:15:40 +0000)

### Fixes

- [NTP]"Time.Status" remains "Unsynchronized" after Disabling then Enabling "Time."

## Release v2.7.5 - 2024-10-02(18:47:20 +0000)

### Fixes

- [time-manager] Rework Time and Time.Client status

## Release v2.7.4 - 2024-09-19(14:26:32 +0000)

### Fixes

- [tr181-time] time manager fails to synchronize time
- tr181-time: time manager fails to synchronize time

## Release v2.7.3 - 2024-09-10(14:24:47 +0000)

### Other

- [AppArmor] Create AppAmor profile for plugins

## Release v2.7.2 - 2024-09-10(14:15:30 +0000)

### Fixes

- [time-manager] Sync fails when in sequential mode

## Release v2.7.1 - 2024-09-09(10:48:23 +0000)

### Other

- - Improve the way time-manager updates chrony daemon
- - Time.Status takes more than 90secs to get Synchronized state at startup
- - time manager fails to synchronize time

## Release v2.7.0 - 2024-09-05(11:19:32 +0000)

### Security

- PRPL/COVERITY: time-manager issue

## Release v2.6.9 - 2024-08-30(14:09:40 +0000)

### Fixes

- [PrplOs][tr181-time] Time.Status takes more than 90secs to get Synchronized state at startup.
- [regression][time] automated test started failing
- [PRPL][4.0.16.8][System > Date & Time, Automatic Time Update]Changing time zone or Enabling/Disabling Automatic Time Update > Displays 'Something went wrong'
- [time] cpu usage of chrony 38.6% / unbound 48.8% when wan is down

## Release v2.6.8 - 2024-07-29(06:10:23 +0000)

### Fixes

- [PrplOs][tr181-time] Time.Status takes more than 90secs to get Synchronized state at startup.

## Release v2.6.7 - 2024-07-23(07:44:52 +0000)

### Fixes

- Better shutdown script

## Release v2.6.6 - 2024-07-18(08:48:43 +0000)

### Fixes

- [PrplOs][tr181-time] Time.Status takes more than 90secs to get Synchronized state at startup.

## Release v2.6.5 - 2024-07-08(07:09:16 +0000)

## Release v2.6.4 - 2024-07-05(10:00:26 +0000)

### Other

- -TR-181: Device.Time data model issues 19.03.2024

## Release v2.6.3 - 2024-05-16(07:14:58 +0000)

### Other

- create Time.Server objects depending on the number of bridges

## Release v2.6.2 - 2024-04-10(10:02:04 +0000)

### Changes

- Make amxb timeouts configurable

## Release v2.6.1 - 2024-04-02(14:14:23 +0000)

### Other

- [TR181-Time] The Home CPE SHALL have the ability to log all manually entered changes to the system clock.

## Release v2.6.0 - 2024-03-23(13:11:13 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v2.5.1 - 2024-01-24(09:20:58 +0000)

### Fixes

- Validate the reason a validation function is called

## Release v2.5.0 - 2023-12-07(10:13:41 +0000)

### New

- [TR181-Time] Add Function to set custom time

## Release v2.4.5 - 2023-12-07(09:57:39 +0000)

### Other

- [tr181-time] Status keeps synchronized when not synchronized

## Release v2.4.4 - 2023-11-23(14:10:14 +0000)

### Other

- support sequential syncing with NTP servers

## Release v2.4.3 - 2023-11-19(12:22:19 +0000)

### Fixes

- - NTP TimeZone is not upgrade persistent

## Release v2.4.2 - 2023-11-02(10:46:34 +0000)

### Changes

- CLONE - Include extensions directory in ODL

## Release v2.4.1 - 2023-10-17(11:00:49 +0000)

### Other

- [Time][DHCPOption] Add the functionality to configure the NTPServers based on the DHCP option

## Release v2.4.0 - 2023-10-16(10:27:27 +0000)

### New

- [amxrt][no-root-user][capability drop] All amx plugins must be adapted to run as non-root and lmited capabilities

## Release v2.3.1 - 2023-10-13(14:10:57 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v2.3.0 - 2023-09-09(06:26:28 +0000)

### New

- [Time][Upgrade Persistent] The Time Server parameter needs to be made upgrade persistent

## Release v2.2.2 - 2023-08-22(07:02:10 +0000)

### Fixes

- - [TR181-Time] Device.Time is miss configured

## Release v2.2.1 - 2023-07-04(15:05:10 +0000)

### Fixes

- Init script has no shutdown function

## Release v2.2.0 - 2023-06-07(08:23:45 +0000)

### New

- add ACLs permissions for cwmp user

## Release v2.1.20 - 2023-05-25(10:01:51 +0000)

### Other

- - [HTTPManager][WebUI] Create plugin's ACLs permissions

## Release v2.1.19 - 2023-04-20(15:09:30 +0000)

### Fixes

- service protocol should be integers

## Release v2.1.18 - 2023-04-17(11:29:56 +0000)

### Fixes

- [odl]Remove deprecated odl keywords

## Release v2.1.17 - 2023-03-21(14:02:08 +0000)

### Fixes

- [tr181-dhcpv4][tr181-ppp][tr181-time] unit test code lines are counted in coverage report

## Release v2.1.16 - 2023-03-17(18:39:58 +0000)

### Other

- [baf] Correct typo in config option

## Release v2.1.15 - 2023-03-16(13:49:21 +0000)

### Other

- Add AP config files

## Release v2.1.14 - 2023-03-09(12:01:16 +0000)

### Other

- Add missing runtime dependency on rpcd
- [Config] enable configurable coredump generation

## Release v2.1.13 - 2023-01-12(10:01:33 +0000)

### Changes

- [time-manager] Excessive chronyd restarts

## Release v2.1.12 - 2023-01-09(13:48:37 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v2.1.11 - 2023-01-09(09:26:33 +0000)

### Fixes

- Fix typos in odl ...
- init without clean on local variables

## Release v2.1.10 - 2022-12-16(08:12:05 +0000)

### Changes

- Excessive chronyd restarts

## Release v2.1.9 - 2022-12-09(09:31:47 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v2.1.8 - 2022-12-02(12:44:08 +0000)

### Changes

- [prpl][Timeplugin] The timeserver must not start until the Time client has synced the time correctly.

## Release v2.1.7 - 2022-11-24(11:13:46 +0000)

### Changes

- [tr181-time] create unit tests for uci-chrony module

## Release v2.1.6 - 2022-11-15(14:01:47 +0000)

### Changes

- [tr181-time] BBF Time Manager Datamodel Evolutions: Time.Server.

## Release v2.1.5 - 2022-10-27(10:09:40 +0000)

### Changes

- [tr181-time] BBF Time Manager Datamodel Evolutions: Time.Client.

## Release v2.1.4 - 2022-09-15(08:07:18 +0000)

### Changes

- [tr181-time] create more unit tests for time-manager

## Release v2.1.3 - 2022-08-25(08:28:10 +0000)

### Changes

- [tr181-time] NTP server on wnc not reachable

## Release v2.1.2 - 2022-07-28(08:02:49 +0000)

### Other

- [tr181-time] localtime not changing when timezone changes

## Release v2.1.1 - 2022-06-09(07:34:30 +0000)

### Fixes

- - [tr181-time] remove debug trace

## Release v2.1.0 - 2022-05-27(08:21:53 +0000)

### New

- tr181-time: rewrite time plugin

## Release v2.0.1 - 2022-03-24(10:57:13 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v2.0.0 - 2022-03-17(15:44:32 +0000)

### Breaking

- The Time plugin does not configure the firewall correctly.

## Release v1.2.6 - 2022-02-25(11:18:04 +0000)

### Other

- Enable core dumps by default

## Release v1.2.5 - 2022-02-03(18:12:04 +0000)

## Release v1.2.4 - 2022-01-26(21:07:11 +0000)

### Fixes

- Only use index paths when opening NetModel queries

## Release v1.2.3 - 2021-12-23(07:56:52 +0000)

### Fixes

- Remove dictionary

## Release v1.2.2 - 2021-12-13(17:17:57 +0000)

### Changes

- Plugins should only start when wan is up

## Release v1.2.1 - 2021-12-11(08:25:14 +0000)

### Fixes

- Use interface name instead of device name for chronyd

## Release v1.2.0 - 2021-11-04(11:46:26 +0000)

### New

- [tr181-time] add guest config

## Release v1.1.6 - 2021-10-28(07:57:13 +0000)

### Fixes

- Time.CurrentLocalTime is always displayed in UTC time zone

## Release v1.1.5 - 2021-10-13(11:53:14 +0000)

### Fixes

- set start up order to 30

## Release v1.1.4 - 2021-10-06(07:44:50 +0000)

### Fixes

- [Time]Time plugin sometimes does not start

## Release v1.1.3 - 2021-09-29(11:42:42 +0000)

### Fixes

- Missing mod-sahtrace dependecy in some components
- [Time]Time plugin sometimes does not start

## Release v1.1.2 - 2021-08-25(14:20:06 +0000)

### Fixes

- Update DM accordingly to initially populated objects

## Release v1.1.1 - 2021-08-24(13:45:46 +0000)

### Fixes

- open/close firewall port for ntp

## Release v1.1.0 - 2021-08-24(11:54:56 +0000)

### New

- open/close firewall port for ntp

## Release v1.0.6 - 2021-08-23(14:32:26 +0000)

### Fixes

- Align time-manager with latest amx libraries

### Other

- Correct changelog

## Release v1.0.5 - 2021-07-06(11:54:56 2021 +0000)

### Changes

- PCF-147 Overwrite UCI configuration with ODL values on plugin start
- \#14 Remove all printf from source

### Fixes

- PCF-152 generate chronyd configuration on boot

## Release v1.0.4 - 2021-06-22(14:41:11 +0000)

### Changes

- PCF-139 Remove state snapshot and operate on DM directly

### Fixes

- PCF-127 Change in Time.LocalTimzeZone TR181 parameter is not reflected in the Time.CurrentLocalTime update
- \#9 Set correct default values in ODL and remove duplication to DM object
- \#8 Check fscanf return value
- \#10 Dangerous clean target for all tr181 components

## Release v1.0.3 - 2021-05-20(14:40:13 +0000)

### New

- NTP Server control functionality added

### Fixes

- \#4 "Compilation error when use arch64_cortex-a53_musl toolchain"

## Release v1.0.2 - 2021-05-11(13:07:04 +0000)

### New

- NTP Client control functionality added

## Release v1.0.1 - 2021-04-14(15:20:00 +0100)

### New

- Initial release
