all: $(TARGET)

run: $(TARGET)
	set -o pipefail; valgrind --leak-check=full --exit-on-first-error=yes --error-exitcode=1 ./$< 2>&1 | tee -a $(OBJDIR)/unit_test_results.txt;

$(TARGET): $(OBJECTS) $(CFG_OBJECTS) $(CLIENT_OBJECTS) $(SERVER_OBJECTS)
	$(CC) -o $@ $(OBJECTS) $(CFG_OBJECTS) $(CLIENT_OBJECTS) $(SERVER_OBJECTS) $(LDFLAGS) -fprofile-arcs -ftest-coverage

-include $(OBJECTS:.o=.d)
-include $(CFG_OBJECTS:.o=.d)
-include $(CLIENT_OBJECTS:.o=.d)
-include $(SERVER_OBJECTS:.o=.d)

$(OBJDIR)/%.o: ./%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/%.o: $(SRCDIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/configuration/%.o: $(SRCDIR)/configuration/%.c | $(OBJDIR)/configuration
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/client/%.o: $(SRCDIR)/client/%.c | $(OBJDIR)/client
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/server/%.o: $(SRCDIR)/server/%.c | $(OBJDIR)/server
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/%.o: $(MOCK_SRC_DIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS)  -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/%.o: $(TEST_UTILS_SRC_DIR)/%.c | $(OBJDIR)/
	$(CC) $(CFLAGS)  -c -o $@ $<
	@$(CC) $(CFLAGS) -MM -MP -MT '$(@) $(@:.o=.d)' -MF $(@:.o=.d) $(<)

$(OBJDIR)/:
	mkdir -p $@

$(OBJDIR)/configuration:
	mkdir -p $@

$(OBJDIR)/client:
	mkdir -p $@

$(OBJDIR)/server:
	mkdir -p $@

clean:
	rm -f $(TARGET) $(OBJDIR)

.PHONY: clean $(OBJDIR)/
