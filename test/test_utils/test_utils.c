/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include "time-manager.h"
#include "dm_time-manager.h"
#include "client/time-manager_client.h"
#include "client/dm_time-manager_client.h"
#include "server/dm_time-manager_server.h"
#include "test_utils.h"

#include <amxut/amxut_macros.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

static amxp_signal_t* signal_handler = NULL;

static const char* test_odl_defs = "../test_utils/time-manager_test.odl";

amxd_dm_t* test_get_dm(void) {
    return amxut_bus_dm();
}

amxo_parser_t* test_get_parser(void) {
    return amxut_bus_parser();
}

int test_time_plugin_teardown(UNUSED void** state) {
    _time_main(AMXO_STOP, amxut_bus_dm(), amxut_bus_parser());

    amxp_signal_delete(&signal_handler);

    amxut_bus_teardown(NULL);

    return 0;
}

int test_time_plugin_setup(UNUSED void** state) {
    amxut_bus_setup(NULL);
    sahTraceSetLevel(TRACE_LEVEL_CALLSTACK);
    sahTraceAddZone(TRACE_LEVEL_CALLSTACK, "time-main");

    assert_int_equal(amxp_signal_new(NULL, &signal_handler, strsignal(SIGCHLD)), 0);

    amxut_resolve_function("print_event", _print_event);
    amxut_resolve_function("time_toggle", _time_toggle);
    amxut_resolve_function("dm_client_toggle", _dm_client_toggle);
    amxut_resolve_function("dm_client_added", _dm_client_added);
    amxut_resolve_function("dm_client_changed", _dm_client_changed);
    amxut_resolve_function("dm_client_interface_changed", _dm_client_interface_changed);
    amxut_resolve_function("update_status", _update_status);
    amxut_resolve_function("SetTime", _SetTime);
    amxut_resolve_function("update_local_timezone", _update_local_timezone);
    amxut_resolve_function("read_current_local_time", _read_current_local_time);
    amxut_resolve_function("check_local_timezone", _LocalTimeZone_check_local_timezone);
    amxut_resolve_function("server_added", _server_added);
    amxut_resolve_function("server_changed", _server_changed);
    amxut_resolve_function("server_interface_changed", _server_interface_changed);
    amxut_resolve_function("count_changed", _count_changed);

    amxut_dm_load_odl(test_odl_defs);

    amxc_var_add_key(amxc_htable_t, &(amxut_bus_parser()->config), "time", NULL);

    _time_main(AMXO_START, amxut_bus_dm(), amxut_bus_parser());

    test_time_handle_events();

    return 0;
}

void test_time_handle_events(void) {
    amxut_bus_handle_events();
}

bool test_time_enable_is(const bool state) {
    amxd_dm_t* dm = test_get_dm();
    amxd_object_t* time_plugin = NULL;

    assert_non_null(dm);
    time_plugin = amxd_dm_findf(dm, "Time");
    assert_non_null(time_plugin);

    return state == amxd_object_get_value(bool, time_plugin, "Enable", NULL);
}

void test_time_set_enable(bool state) {
    amxd_dm_t* dm = test_get_dm();
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);

    amxd_trans_select_pathf(&transaction, "Time.");
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_value(bool, &transaction, "Enable", state);
    amxd_trans_apply(&transaction, dm);
    test_time_handle_events();

    amxd_trans_clean(&transaction);
}

void test_time_set_status(const char* value) {
    assert_non_null(value);
    amxd_dm_t* dm = test_get_dm();
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);

    amxd_trans_select_pathf(&transaction, "Time.");
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &transaction, "Status", value);
    amxd_trans_apply(&transaction, dm);
    test_time_handle_events();

    amxd_trans_clean(&transaction);
}

void test_time_server_set_enable(bool state, uint32_t index) {
    amxd_dm_t* dm = test_get_dm();
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);

    amxd_trans_select_pathf(&transaction, "Time.Server.%d.", index);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_value(bool, &transaction, "Enable", state);
    amxd_trans_apply(&transaction, dm);
    test_time_handle_events();

    amxd_trans_clean(&transaction);
}

void test_time_client_set_enable(bool state, uint32_t index) {
    amxd_dm_t* dm = test_get_dm();
    amxd_trans_t transaction;
    amxd_trans_init(&transaction);

    amxd_trans_select_pathf(&transaction, "Time.Client.%d.", index);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_set_value(bool, &transaction, "Enable", state);
    amxd_trans_apply(&transaction, dm);
    test_time_handle_events();

    amxd_trans_clean(&transaction);
}

void test_time_read_sigalrm(void) {
    sigset_t mask;
    int sfd;
    struct signalfd_siginfo fdsi;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);

    sigprocmask(SIG_BLOCK, &mask, NULL);

    sfd = signalfd(-1, &mask, 0);
    s = read(sfd, &fdsi, sizeof(struct signalfd_siginfo));
    assert_int_equal(s, sizeof(struct signalfd_siginfo));
    if(fdsi.ssi_signo == SIGALRM) {
        printf("Got SIGALRM\n");
    } else {
        printf("Read unexpected signal\n");
    }
}

