/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <time.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>

#include <amxut/amxut_dm.h>

#include "test_utils.h"

#include "utils.h"
#include "test_datamodel.h"

#include "time-manager.h"
#include "configuration/time_configuration.h"
#include "client/time-manager_client.h"
#include "client/ntp_object.h"
#include "client/ntp_state.h"
#include "ntp_daemon/service.h"
#include "mock_time.h"


static int time_dm_invoke(const char* func, amxc_var_t* args) {
    int rv = -1;
    amxd_object_t* time = time_dm_get_object("Time");
    amxc_var_t ret;
    amxc_var_init(&ret);

    rv = amxd_object_invoke_function(time, func, args, &ret);
    assert_true(amxc_var_is_null(&ret));
    test_time_handle_events();

    amxc_var_clean(&ret);
    return rv;
}

void test_ntp_toggle(UNUSED void** state) {
    test_time_set_enable(false);
    test_time_handle_events();
}

void test_ntp_set_time(UNUSED void** state) {
    amxc_ts_t ts = {.sec = 1701346693, .nsec = 9000000};
    amxc_var_t args;
    amxc_var_init(&args);

    amxut_dm_param_equals(cstring_t, "Time", "Status", "Disabled");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(amxc_ts_t, &args, "Time", &ts);

    expect_value(__wrap_clock_settime, sec, ts.sec);

    assert_int_equal(time_dm_invoke("SetTime", &args), amxd_status_ok);

    amxut_dm_param_equals(cstring_t, "Time", "Status", "Synchronized");

    amxc_var_clean(&args);
}

void test_ntp_set_time_and_timezone(UNUSED void** state) {
    amxc_ts_t ts = {.sec = 1701346693, .nsec = 9000000};
    amxc_var_t args;
    amxc_var_init(&args);

    amxut_dm_param_equals(cstring_t, "Time", "LocalTimeZone", "GMT0");

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(amxc_ts_t, &args, "Time", &ts);
    amxc_var_add_key(cstring_t, &args, "TimeZone", "CET-1");

    expect_value(__wrap_clock_settime, sec, ts.sec);

    assert_int_equal(time_dm_invoke("SetTime", &args), amxd_status_ok);

    amxut_dm_param_equals(cstring_t, "Time", "LocalTimeZone", "CET-1");

    amxc_var_clean(&args);
}

void test_ntp_server_changed(UNUSED void** state) {
    amxd_object_t* time = NULL;
    amxc_var_t params;

    time = time_dm_get_object("Time");

    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &params, "NTPServer1", "test123");
    amxd_object_send_changed(time, &params, false);
    amxc_var_clean(&params);

    test_time_handle_events();
}

void test_update_time_and_clients_status(UNUSED void** state) {
    amxd_object_t* time = time_dm_get_object("Time");

    test_time_set_enable(false);
    test_time_handle_events();

    //todo: check datamodel for Time.Status == unsync and Time.Client.i.Status = Error
    update_time_and_clients_status(time);
}

void test_ntp_service_start(UNUSED void** state) {
    assert_true(ntp_service_start());
}
