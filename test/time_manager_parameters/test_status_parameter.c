/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>

#include <amxo/amxo.h>

#include "time-manager.h"
#include "client/ntp_state.h"
#include "client/ntp_object.h"
#include "test_utils.h"
#include <amxut/amxut_timer.h>

#include "test_status_parameter.h"

void test_status_parameter(UNUSED void** state) {
    char* status = NULL;
    amxd_object_t* time = amxd_dm_findf(test_get_dm(), "Time");
    assert_non_null(time);

    test_time_set_enable(false);
    assert_true(test_time_enable_is(false));
    amxut_timer_go_to_future_ms(2100);

    status = amxd_object_get_value(cstring_t, time, "Status", NULL);
    assert_non_null(status);
    assert_string_equal("Disabled", status);
    free(status);

    test_time_set_enable(true);
    assert_true(test_time_enable_is(true));
    amxut_timer_go_to_future_ms(2100);

    status = amxd_object_get_value(cstring_t, time, "Status", NULL);
    assert_non_null(status);
    assert_string_equal("Unsynchronized", status);
    free(status);

    test_time_set_status("Synchronized");
    status = amxd_object_get_value(cstring_t, time, "Status", NULL);
    assert_non_null(status);
    assert_string_equal("Synchronized", status);
    free(status);

    test_time_set_status("Error");
    status = amxd_object_get_value(cstring_t, time, "Status", NULL);
    assert_non_null(status);
    assert_string_equal("Error", status);
    free(status);

    test_time_set_enable(false);
    assert_true(test_time_enable_is(false));
    amxut_timer_go_to_future_ms(2100);

    status = amxd_object_get_value(cstring_t, time, "Status", NULL);
    assert_non_null(status);
    assert_string_equal("Disabled", status);
    free(status);
}
