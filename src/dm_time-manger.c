/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_action.h>
#include "dm_time-manager.h"
#include "client/time-manager_client.h"
#include "server/time-manager_server.h"
#include "server/firewall.h"
#include "client/ntp_state.h"
#include "client/ntp_object.h"
#include "ntp_daemon/service.h"
#include "timezone.h"
#include "time-manager_soc_utils.h"
#include "configuration/time_configuration.h"
#include <amxd/amxd_action.h>
#include <time.h>

static amxd_status_t dm_time_set_local_timezone(amxd_object_t* object, const cstring_t tz) {
    amxd_status_t rv;
    amxd_trans_t trans;
    amxd_trans_init(&trans);

    rv = amxd_trans_select_object(&trans, object);
    when_failed(rv, exit);

    rv = amxd_trans_set_cstring_t(&trans, "LocalTimeZone", tz);
    when_failed(rv, exit);

    rv = amxd_trans_apply(&trans, time_get_dm());
exit:
    amxd_trans_clean(&trans);
    return rv;
}

static amxd_status_t dm_time_set_manual(amxd_object_t* object, bool manual) {
    amxd_status_t rv;
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    rv = amxd_trans_select_object(&trans, object);
    when_failed(rv, exit);

    rv = amxd_trans_set_bool(&trans, "Manual", manual);
    when_failed(rv, exit);

    rv = amxd_trans_apply(&trans, time_get_dm());
exit:
    amxd_trans_clean(&trans);
    return rv;
}

void _time_toggle(const char* const event_name UNUSED,
                  const amxc_var_t* const event_data,
                  UNUSED void* const priv) {
    amxd_object_t* time_object = NULL;
    amxd_object_t* server_object = NULL;
    bool enabled = false;

    time_object = amxd_dm_signal_get_object(time_get_dm(), event_data);
    when_null(time_object, exit);
    update_time_config_enable(time_object);
    dm_time_set_manual(time_object, false);

    server_object = amxd_dm_findf(time_get_dm(), "Time.Server.");
    when_null(server_object, exit);
    amxd_object_for_each(instance, it, server_object) {
        amxd_object_t* server = amxc_container_of(it, amxd_object_t, it);
        time_server_refresh(server);
    }

    enabled = time_dm_get_enable(time_object);
    if(!enabled) {
        ntp_service_terminate();
        firewall_close_ntp_port();
    }

    time_config_update(time_object);
    ntp_update_sync_timer(0);

exit:
    return;
}

void _update_local_timezone(UNUSED const char* const event_name,
                            const amxc_var_t* const event_data,
                            UNUSED void* const priv) {
    amxd_object_t* time_object = NULL;
    time_object = amxd_dm_signal_get_object(time_get_dm(), event_data);
    when_null(time_object, exit);
    time_config_update(time_object);
exit:
    return;
}


amxd_status_t _update_status(amxd_object_t* object,
                             UNUSED amxd_function_t* func,
                             UNUSED amxc_var_t* args,
                             UNUSED amxc_var_t* ret) {
    amxd_status_t rc = amxd_status_ok;

    update_time_and_clients_status(object);

    return rc;
}

amxd_status_t _SetTime(amxd_object_t* object,
                       UNUSED amxd_function_t* func,
                       amxc_var_t* args,
                       UNUSED amxc_var_t* ret) {
    amxd_status_t rv = amxd_status_unknown_error;
    int res = 0;
    const cstring_t tz = GET_CHAR(args, "TimeZone");
    amxc_ts_t* ts = amxc_var_dyncast(amxc_ts_t, GET_ARG(args, "Time"));

    rv = amxd_status_invalid_arg;

    when_null_trace(ts, exit, ERROR, "Time arg missing or invalid");

    if(tz != NULL) {
        rv = dm_time_set_local_timezone(object, tz);
        when_failed_trace(rv, exit, ERROR, "Failed to set LocalTimeZone=%s", tz);
    }

    rv = amxd_status_unknown_error;

    res = set_system_time(ts);
    when_failed_trace(res, exit, ERROR, "Failed to set system time: %d", res);
    SAH_TRACEZ_WARNING(ME, "System time set manually");

    res = dm_time_set_manual(object, true);
    when_failed_trace(res, exit, ERROR, "Failed to set Time.Manual: %d", res);
    update_time_and_clients_status(object);

    rv = amxd_status_ok;
exit:
    free(ts);
    return rv;
}

amxd_status_t _LocalTimeZone_check_local_timezone(UNUSED amxd_object_t* object,
                                                  amxd_param_t* param,
                                                  UNUSED amxd_action_t reason,
                                                  const amxc_var_t* const args,
                                                  UNUSED amxc_var_t* const retval,
                                                  UNUSED void* priv) {
    amxd_status_t status = amxd_status_invalid_value;

    const char* current_value = amxc_var_constcast(cstring_t, &param->value);
    const char* new_value = GET_CHAR(args, NULL);

    when_null(current_value, exit);
    when_null(new_value, exit);

    when_true_status(reason != action_param_validate, exit, status = amxd_status_function_not_implemented);

    if((0 == strcmp(current_value, new_value)) || is_posix_timezone(new_value)) {
        status = amxd_status_ok;
    }

exit:
    return status;
}

amxd_status_t _read_current_local_time(UNUSED amxd_object_t* object,
                                       UNUSED amxd_param_t* param,
                                       UNUSED amxd_action_t reason,
                                       UNUSED const amxc_var_t* const args,
                                       UNUSED amxc_var_t* const retval,
                                       UNUSED void* priv) {
    amxc_ts_t now;
    amxc_ts_now(&now);

    amxd_object_t* time_object = amxd_dm_findf(time_get_dm(), "Time.");
    when_null(time_object, exit);
    const char* timezone = amxc_var_constcast(cstring_t, amxd_object_get_param_value(time_object, "LocalTimeZone"));
    setenv("TZ", timezone, 1);
    tzset();

exit:
    amxc_ts_to_local(&now);

    amxc_var_set(amxc_ts_t, &param->value, &now);
    return amxd_action_param_read(object,
                                  param,
                                  reason,
                                  args,
                                  retval,
                                  priv);

}

void _count_changed(const char* const event_name UNUSED,
                    UNUSED const amxc_var_t* const event_data,
                    UNUSED void* const priv) {
    amxd_object_t* time_object = NULL;
    time_object = amxd_dm_findf(time_get_dm(), "Time.");
    when_null(time_object, exit);
    time_config_update(time_object);

exit:
    return;
}

