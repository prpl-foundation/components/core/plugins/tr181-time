/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include "time-manager.h"
#include "server/firewall.h"

#define FIREWALL_OBJECT "Firewall."

static amxc_var_t* services = NULL;  // database for key value pairs: interface (key) / Firewall.X_Prpl_Service.Alias

int firewall_open_ntp_port(UNUSED amxd_object_t* templ, amxd_object_t* instance, UNUSED void* priv) {
    amxc_var_t args;
    amxc_var_t ret;
    const char* interface = amxc_var_constcast(cstring_t, amxd_object_get_param_value(instance, "Interface"));
    amxc_string_t _alias;
    const char* alias = NULL;

    when_str_empty(interface, exit);

    SAH_TRACEZ_INFO(ME, "opening firewall port 123 on interface[%s]", interface);

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_string_init(&_alias, 0);

    if(services == NULL) {
        if(amxc_var_new(&services) != 0) {
            SAH_TRACEZ_WARNING(ME, "failed to create database");
            goto cleanup;
        }
        amxc_var_set_type(services, AMXC_VAR_ID_HTABLE);
    } else if(amxc_var_get_key(services, interface, AMXC_VAR_FLAG_DEFAULT) != NULL) {
        SAH_TRACEZ_WARNING(ME, "found existing firewall service with interface[%s]", interface);
        goto cleanup;
    }

    amxc_string_appendf(&_alias, "time-%s", amxd_object_get_name(instance, AMXD_OBJECT_NAMED));
    alias = amxc_string_get(&_alias, 0);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "alias", alias);
    amxc_var_add_key(cstring_t, &args, "interface", interface);
    amxc_var_add_key(cstring_t, &args, "protocol", "17,6");
    amxc_var_add_key(uint32_t, &args, "destinationPort", 123);
    amxc_var_add_key(bool, &args, "enable", true);

    if(AMXB_STATUS_OK != amxb_call(amxb_be_who_has(FIREWALL_OBJECT), FIREWALL_OBJECT, "setService", &args, &ret, 5)) {
        SAH_TRACEZ_ERROR(ME, "failed to open firewall port on interface[%s]", interface);
        goto cleanup;
    }

    amxc_var_add_key(cstring_t, services, interface, alias);
    SAH_TRACEZ_INFO(ME, "added firewall service[%s]", alias);

cleanup:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
    amxc_string_clean(&_alias);
exit:
    return 1;
}

bool firewall_close_ntp_port(void) {
    SAH_TRACEZ_INFO(ME, "closing firewall port 123");

    amxc_var_for_each(var, services) {
        amxc_var_t args;
        amxc_var_t ret;
        const char* alias = amxc_var_constcast(cstring_t, var);

        amxc_var_init(&args);
        amxc_var_init(&ret);

        amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
        amxc_var_add_key(cstring_t, &args, "alias", alias);

        if(AMXB_STATUS_OK != amxb_call(amxb_be_who_has(FIREWALL_OBJECT), FIREWALL_OBJECT, "deleteService", &args, &ret, 5)) {
            SAH_TRACEZ_ERROR(ME, "failed to remove firewall service[%s]", alias);
            continue;
        }

        SAH_TRACEZ_INFO(ME, "removed firewall service[%s]", alias);

        amxc_var_clean(&args);
        amxc_var_clean(&ret);
    }

    // all key value pairs have been handled, destroy the database
    amxc_var_delete(&services);

    return true;
}
