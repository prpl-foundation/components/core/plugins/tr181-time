/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "time-manager.h"
#include "client/time-manager_client.h"
#include "server/dm_time-manager_server.h"
#include "client/ntp_object.h"
#include "configuration/time_configuration.h"
#include <amxm/amxm.h>

static time_app_t app;

void _print_event(UNUSED const char* const sig_name,
                  UNUSED const amxc_var_t* const data,
                  UNUSED void* const priv) {
    printf("event received - %s\n", sig_name);
    if(data != NULL) {
        printf("Event data = \n");
        fflush(stdout);
        amxc_var_dump(data, STDOUT_FILENO);
    }
}

amxd_dm_t* PRIVATE time_get_dm(void) {
    return app.dm;
}

amxo_parser_t* PRIVATE time_get_parser(void) {
    return app.parser;
}

amxd_object_t* PRIVATE time_dm_get_object(const char* dm_object) {
    return amxd_dm_findf(time_get_dm(), "%s", dm_object);
}

static int time_mngr_load_time_controllers(void) {
    int retval = -1;
    amxd_object_t* time = time_dm_get_object("Time");
    const amxc_var_t* modules = amxd_object_get_param_value(time, "SupportedTimeControllers");
    const char* mod_dir = amxc_var_constcast(cstring_t, amxd_object_get_param_value(time, "mod-dir"));

    amxc_var_t lmodules;
    amxm_shared_object_t* so = NULL;
    amxc_string_t mod_path;

    amxc_var_init(&lmodules);
    amxc_string_init(&mod_path, 0);

    when_null(time, exit);
    when_null(mod_dir, exit);

    amxc_var_convert(&lmodules, modules, AMXC_VAR_ID_LIST);
    amxc_var_for_each(module, &lmodules) {
        const char* name = GET_CHAR(module, NULL);
        amxc_string_setf(&mod_path, "%s/%s.so", mod_dir, name);
        SAH_TRACEZ_INFO(ME, "Loading module '%s' (file = %s)", name, amxc_string_get(&mod_path, 0));
        retval = amxm_so_open(&so, name, amxc_string_get(&mod_path, 0));
        when_failed_trace(retval, exit, ERROR, "failed(%d) to open module %s", retval, name);
    }


exit:
    amxc_string_clean(&mod_path);
    amxc_var_clean(&lmodules);
    return retval;
}

bool time_is_initialized(void) {
    return app.init_done;
}

int _time_main(int reason,
               amxd_dm_t* dm,
               amxo_parser_t* parser) {

    amxc_var_t* eventing = NULL;
    bool orig_eventing = false;

    switch(reason) {
    case 0:                              // START

        app.dm = dm;
        app.parser = parser;
        app.init_done = false; // Do not allow the actual ntp implementation to start
        eventing = GET_ARG(&parser->config, "dm-eventing-enabled");
        orig_eventing = GET_BOOL(eventing, NULL);
        amxc_var_set(bool, eventing, false);
        amxp_sigmngr_enable(&dm->sigmngr, false); // Disable events while loading odl
        amxo_parser_parse_string(parser, "?include '${odl.directory}/${name}.odl':'${odl.dm-defaults}';",
                                 amxd_dm_get_root(dm));
        amxp_sigmngr_enable(&dm->sigmngr, true); // Enable events again
        amxc_var_set(bool, eventing, orig_eventing);

        time_mngr_load_time_controllers();
        ntp_init();
        time_init(&app);
        server_init(&app);
        app.init_done = true; // Allow the implementation to start
        time_config_init();   // Check all parameters and start the sync if necessary
        break;
    case 1:                   // STOP
        time_cleanup();
        server_cleanup();
        time_config_cleanup();
        ntp_cleanup();
        app.dm = NULL;
        app.parser = NULL;
        amxm_close_all();

        break;
    }
    return 0;
}


