/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdlib.h>

#include <amxc/amxc.h>
#include <stdio.h>

#include "time-manager.h"
#include "timezone.h"
#include "ntp_daemon/service.h"
#include "client/time-manager_client.h"
#include "client/ntp_object.h"
#include "configuration/time_configuration.h"

#include "server/time-manager_server.h"

static amxd_status_t update_client_dm_status_parameter(const char* value, amxd_object_t* client) {
    amxd_status_t rc = amxd_status_unknown_error;
    amxd_trans_t transaction;
    when_null(client, exit);
    when_str_empty(value, exit);

    amxd_trans_init(&transaction);
    amxd_trans_set_attr(&transaction, amxd_tattr_change_ro, true);
    amxd_trans_select_object(&transaction, client);
    amxd_trans_set_value(cstring_t, &transaction, "Status", value);

    rc = amxd_trans_apply(&transaction, time_get_dm());
    amxd_trans_clean(&transaction);

exit:
    return rc;
}

static amxd_status_t time_disable_all_clients(amxd_object_t* time_object) {
    amxd_status_t ret = amxd_status_ok;
    bool changed = false;
    amxd_object_t* clients = NULL;

    when_null(time_object, exit);

    clients = amxd_object_get_child(time_object, "Client");
    when_null(clients, exit);

    amxd_object_for_each(instance, iter, clients) {
        amxd_object_t* client = amxc_llist_it_get_data(iter, amxd_object_t, it);
        when_null(client, next);
        ntp_sync_state_t state = sync_state_from_string(time_dm_get_status(client));
        changed = ntp_sync_state(&state, time_object, client);
        const char* client_status = sync_state_to_string(state);
        if(changed) {
            update_client_dm_status_parameter(client_status, client);
        }
next:
        continue;
    }
exit:
    return ret;
}

void update_time_and_clients_status(amxd_object_t* time_object) {
    amxd_status_t ret = amxd_status_ok;
    bool time_enabled = false;
    bool time_manual = false;
    bool clients_enabled = false;
    bool clients_synchronized = false;
    bool clients_unsynchronized = false;
    bool clients_failed = false;
    bool changed = false;
    amxd_object_t* clients = NULL;
    const char* status = NULL;

    when_null(time_object, exit);
    time_manual = amxd_object_get_value(bool, time_object, "Manual", NULL);

    time_enabled = time_dm_get_enable(time_object);
    if(!time_enabled) {
        ret = time_disable_all_clients(time_object);
        when_failed_trace(ret, exit, ERROR, "Failed to update one or more clients");
        goto exit;
    }

    clients = amxd_object_get_child(time_object, "Client");
    when_null(clients, exit);

    amxd_object_for_each(instance, iter, clients) {
        const char* client_status = NULL;
        amxd_object_t* client = amxc_llist_it_get_data(iter, amxd_object_t, it);
        ntp_sync_state_t state = Unsynchronized;
        when_null(client, next);
        changed = ntp_sync_state(&state, time_object, client);
        client_status = sync_state_to_string(state);

        switch(state) {
        case Synchronized:
            clients_synchronized = true;
            break;
        case Unsynchronized:
            clients_unsynchronized = true;
            break;
        case Error:
            clients_failed = true;
            break;
        default:
            break;
        }
        if(changed) {
            update_client_dm_status_parameter(client_status, client);
        }
        if(time_dm_get_enable(client)) {
            clients_enabled = true;
        }
next:
        continue;
    }

exit:
    if(!time_enabled || !clients_enabled) {
        status = time_manual ? "Synchronized" : "Disabled";
    } else if(clients_synchronized) {
        status = "Synchronized";
    } else if(clients_unsynchronized) {
        if(sync_state_from_string(time_dm_get_status(time_object)) != Synchronized) {
            status = "Unsynchronized";
        }
    } else if(clients_failed) {
        status = "Error";
    } else {
        status = "Unsynchronized";
    }
    update_time_dm_status_parameter(status);
    return;
}

