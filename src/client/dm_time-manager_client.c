/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_action.h>
#include "client/dm_time-manager_client.h"
#include "client/time-manager_client.h"
#include "server/time-manager_server.h"
#include "time_netmodel.h"
#include "timezone.h"
#include "client/ntp_object.h"
#include "configuration/time_configuration.h"
#include <amxd/amxd_action.h>
#include <time.h>

void _dm_client_changed(UNUSED const char* const event_name,
                        UNUSED const amxc_var_t* const event_data,
                        UNUSED void* const priv) {
    amxd_object_t* time_object = amxd_dm_findf(time_get_dm(), "Time.");
    when_null(time_object, exit);
    time_config_update(time_object);
    ntp_update_sync_timer(0);
exit:
    return;
}

amxd_status_t _dm_client_interface_changed(UNUSED const char* const event_name,
                                           const amxc_var_t* const event_data,
                                           UNUSED void* const priv) {
    amxd_status_t status = amxd_status_unknown_error;
    amxd_object_t* client = amxd_dm_signal_get_object(time_get_dm(), event_data);
    when_null(client, exit);

    status = update_client_info(client);
exit:
    return status;
}

void _dm_client_toggle(UNUSED const char* const event_name,
                       UNUSED const amxc_var_t* const event_data,
                       UNUSED void* const priv) {
    amxd_object_t* time_object = NULL;

    time_object = amxd_dm_findf(time_get_dm(), "Time.");
    when_null(time_object, exit);
    time_config_update(time_object);
    ntp_update_sync_timer(0);

exit:
    return;
}

void _dm_client_added(UNUSED const char* const event_name,
                      UNUSED const amxc_var_t* const event_data,
                      UNUSED void* const priv) {
    amxd_object_t* time_object = NULL;
    time_object = amxd_dm_findf(time_get_dm(), "Time.");
    when_null(time_object, exit);
    time_config_update(time_object);
    ntp_update_sync_timer(0);

exit:
    return;
}

amxd_status_t _dm_client_action_on_destroy(amxd_object_t* const client_instance,
                                           amxd_param_t* const param,
                                           amxd_action_t reason,
                                           const amxc_var_t* const args,
                                           amxc_var_t* const action_retval,
                                           void* priv) {
    amxd_object_t* time_object = amxd_dm_findf(time_get_dm(), "Time.");
    amxd_status_t status = amxd_status_ok;
    when_null(time_object, exit);
    status = amxd_action_object_destroy(client_instance, param, reason, args, action_retval, priv);

    time_config_update(time_object);
    ntp_update_sync_timer(0);

exit:
    return status;
}

