%define {
   select Time {
        /**
         * 	Object controlling the Time server.
         *  @version 1.0
         */
         %persistent object Server[] {
            on action destroy call del_server;
            counted with ServerNumberOfEntries;
            /**
             * Enables/disables listening on the specified interface
             *
             * @version 1.0
             */
            %persistent bool Enable {
               default false;
            }
            /**
              * Describes the status of the NTP server for this IP address, one of : "Up", "Down", "Error"
              *
              * @version 1.0
              */
            %read-only string Status {
                on action validate call check_enum ["Up", "Down", "Error"];
                default "Down";
            }
            /**
             * A non-volatile unique key used to reference this instance.
             * @version 1.0
             */
            %unique %key string Alias;
            /**
             *Specifies in which mode the NTP server must be ran. Enumeration of: \n
             *Unicast (Support for the NTP server in unicast mode) \n
             *Broadcast (Support for the NTP server in broadcast mode) \n
             *Multicast (Support for the NTP server in multicast mode)  \n
             *Manycast (Support for the NTP server in manycast mode)
             *@version 1.0
             */
            %persistent string Mode {
                on action validate call check_enum ["Unicast", "Broadcast", "Multicast", "Manycast"];
                default "Unicast";
            }
            /**
             *Specifies the supported NTP version. Possible versions are 1-4.
             *@version 1.0
             */
            %persistent uint32 Version {
                default 4;
            }
            /**
             *Specify the port used to receive NTP packets
             *@version 1.0
             */
            %persistent uint32 Port {
                default 123;
            }
            /**
             *The value MUST be the Path Name of a row in the IP.Interface table.
             *If the referenced object is deleted, the parameter value MUST be set to an empty string. The IP Interface associated with the Server entry.
             *@version 1.0
             */
            %persistent string Interface;
            /**
             *Specifies how the client sockets must be bounded. Enumeration of: \n
             *   Address (The server sockets are bound to a local IP address) \n
             *   Device (The server sockets are bound to a network device. This can be useful when the local address is dynamic)
             *@version 1.0
             */
            %persistent string BindType {
              on action validate call check_enum ["Address", "Device"];
              default "Device";
            }
            /**
             *This is the minimum polling interval, in seconds to the power of two, allowed by any peer of the Internet system, currently set to 6 (64 seconds).
             *@version 1.0
             *
             */
            %persistent uint32 MinPoll {
                default 6;
            }
            /**
             *This is the maximum polling interval, in seconds to the power of two, allowed by any peer of the Internet system, currently set to 10 (1024 seconds)
             *@version 1.0
             */
            %persistent uint32 MaxPoll {
                default 10;
            }
            /**
             *Specifies the time to live (TTL) for a broadcast/multicast packet.
             *@version 1.0
             */
            %persistent uint32 TTL {
                default 255;
            }
            %persistent object Authentication {
                /**
                 * Enables or disables authentication of the NTP server.
                 *
                 * @version 1.0
                */
                %persistent bool Enable {
                    default false;
                }
                /**
                 * The value MUST be the Path Name of a row in the Security.Certificate table.
                 * If the referenced object is deleted, the parameter value MUST be set to an empty string. Points to the certificate that must be used by the NTS-KE client.
                 *
                 * @version 1.0
                */
                %persistent string Certificate;
                /**
                 * Comma-separated list of strings. Points to a CSV list of NTP servers.
                 * A NTP server can either be specified as an IP address or a host name.
                 * When used the NTS-KE server will tell the remote NTS-KE client the NTP hostname or address of the NTP server(s) that should be used.
                 * This allows to seperate the NTP server and NTS-KE server implementation.
                 *
                 * @version 1.0
                */
                %persistent csv_string NTSNTPServer;
            }
            /**
             * This object specifies the statistic parameters for the NTP server.
             *
             * @version 1.0
            */
            %persistent object Stats {
                /**
                 * 	[StatsCounter32] Specifies the number of packets sent by the NTP server.
                 *
                 * @version 1.0
                */
                %read-only uint32 PacketsSent;
                /**
                 * [StatsCounter32] Specifies the number of failed sent packets that the NTP server tried to sent.
                 *
                 * @version 1.0
                */
                %read-only uint32 PacketsSentFailed;
                /**
                 * [StatsCounter32] Specifies the number of received packets.
                 *
                 * @version 1.0
                */
                %read-only uint32 PacketsReceived;
                /**
                 * [StatsCounter32] Specifies the number of dropped packets.
                 *
                 * @version 1.0
                */
                %read-only uint32 PacketsDropped;
            }
       }
   }
}

%populate {
    on event "dm:object-changed" call server_interface_changed
        filter 'path matches "Time\.Server\.[0-9]+\." && contains("parameters.Interface")';
    on event "dm:object-changed" call server_changed
        filter 'path matches "Time\.Server\.[0-9]+\." && !contains("parameters.Status")';

    on event "dm:instance-added" call server_added
        filter 'object == "Time.Server."';

    //on event "*" call print_event;
}
