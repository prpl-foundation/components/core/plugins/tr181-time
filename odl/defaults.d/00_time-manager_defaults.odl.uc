%populate {
    object 'Time' {
        object 'Client' {
            instance add(Alias="cpe-Client-1") {
                parameter BindType = "Device";
{% if (BDfn.hasAnyUpstream()) : %}
                parameter Servers = "0.europe.pool.ntp.org, 1.europe.pool.ntp.org";
{% else %}
                parameter Servers = "192.168.1.1";  
{% endif; %}
                parameter IBurst = true;
                parameter MaxPoll = 12;
                parameter Mode = "Unicast";
                parameter Interface = "Device.Logical.Interface.1.";
            }
        }
    }
{% if (BDfn.hasAnyUpstream()) : %}
    object 'Time.Server' {
{% let i=1 %}
{% for (let Bridge in BD.Bridges) : %}
{% i++ %}
        instance add(Alias="cpe-br-{{lc(Bridge)}}") {
            parameter BindType = "Device";
            parameter Interface = "Device.Logical.Interface.{{i}}.";
            parameter Enable = true;
        }
{% endfor; %}
    }
{% endif; %}
}
